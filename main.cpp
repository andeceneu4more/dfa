#include <fstream>
#include <vector>
#include <string>
#include <cstring>
#include <sstream>
#define nmax 5005
using namespace std;
class tranzitie
{
private:
    int next;
    string c;
public:
    tranzitie(string,int);
    int get_next();
    string get_string();
    friend istream& operator>>(istream&, tranzitie&);
};
tranzitie::tranzitie(string cc="",int urm=0)
{
    c=cc;next=urm;
}
int tranzitie::get_next()
{
    return next;
}
string tranzitie::get_string()
{
    return c;
}
istream& operator>>(istream &in, tranzitie &t)
{
    in>>t.next>>t.c;
    return in;
}
class automata
{
private:
    vector <tranzitie> Q[nmax];
    bool Final[nmax];
    int q0,n;
public:
    automata();
    ~automata();
    void add(int,tranzitie);
    friend istream& operator>>(istream&, automata&);
    int urmator(int,string);
    bool input(string);
};
automata::automata()
{
    memset(Final,false,sizeof(Final));n=q0=0;
}
automata::~automata()
{
    int i;
    for (i=0;i<n;i++)
        Q[i].clear();
    n=q0=0;
    memset(Final,false,sizeof(Final));
}
void automata::add(int x,tranzitie t)
{
    Q[x].push_back(t);
}
istream& operator>>(istream &in, automata &A)
{
    int n,F,i,x;
    tranzitie t;
    string s;
    in>>A.n;
    in.ignore();
    getline(in,s);
    in>>A.q0>>F;
    for (i=1;i<=F;i++)
    {
        in>>x;
        A.Final[x]=1;
    }
    for (in>>n; n; n--)
    {
        in>>x>>t;
        A.add(x,t);
    }
    in.ignore();
    return in;
}
int automata::urmator(int q, string c)
{
    int i,next;string cc;
    for (i=0;i<Q[q].size();i++)
    {
        cc=Q[q][i].get_string();
        next=Q[q][i].get_next();
        if (cc==c)
            return next;
    }
    return -1;
}
bool automata::input(string s)
{
    int i,x=q0;
    istringstream iss(s);
    string p;
    while (getline(iss,p,' ') && (x!=-1))
    {
        x=urmator(x,p);
    }
    if (x==-1)
        return false;
    return Final[x];
}
int main()
{
    ifstream fin("automat.in");
    ofstream fout("automat.out");
    automata A;
    fin>>A;
    string s;
    getline(fin,s);
    fout<<A.input(s)<<'\n';
    fin.close();
    fout.close();
    return 0;
}
